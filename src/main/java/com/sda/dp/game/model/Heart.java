package com.sda.dp.game.model;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Heart extends AbstractGameObject{
    private BufferedImage image;

    public Heart() {
        try{
            image = ImageIO.read(new File("src/main/resources/Heart.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void paint(Graphics2D g2d) {
        g2d.drawImage(image, position.x, position.y, null);
    }

    @Override
    public int getHeight() {
        return image.getHeight();
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

    @Override
    public void hit() {

    }


    public void paint(Graphics2D g2d, int i) {
        position.x = i*getWidth()+i*5;
        position.y = 0;

        paint(g2d);
    }
}
