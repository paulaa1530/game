package com.sda.dp.game.model;

import com.sda.dp.game.dispatcher.Dispatcher;
import com.sda.dp.game.dispatcher.HeroFireEvent;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class GameHero extends AbstractGameObject {

    private BufferedImage image;
    private int healthPoints = 3;

    public GameHero(int x, int y) {
        super(new Point(x, y));
        try {
            image = ImageIO.read(new File("src/main/resources/Spasceship.png"));
        } catch (IOException ex) {
            System.err.println("err");
        }
    }

    @Override
    public void paint(Graphics2D g2d) {
        super.paint(g2d);
        g2d.drawImage(image, position.x, position.y, null);
    }

    @Override
    public int getHeight() {
        return image.getHeight();
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

    @Override
    public void hit() {
        healthPoints--;
    }

    public void fire() {
        //szerokosc obrazka/2 + pozycja postaci-szerokosc strzalu
        int positionX = image.getWidth() / 2 + position.x - 5;

        Dispatcher.instance.dispatch(new HeroFireEvent(positionX, position.y));
    }

    public int getHealthPoints() {
        return healthPoints;
    }
}
