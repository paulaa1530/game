package com.sda.dp.game.model;

import com.sda.dp.game.HorizontalDirection;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Monster extends AbstractGameObject{
    private int hp; //wytrzymalosc na ciosy+durability
    private BufferedImage image;
    private int moveRange;
    private Point initialPosition;

    public Monster(int x, int y, int durability, int moveRange) {
        super(new Point(x, y));
        this.initialPosition = new Point(x, y);
        this.hp = durability;
        this.moveRange= moveRange;
        try{
            image = ImageIO.read(new File("src/main/resources/Invader3.png"));
        } catch (IOException e) {
            System.err.println("Nie mogłem załadować obrazka");
        }
        speed = 1.0;
        hDir = HorizontalDirection.RIGHT;
    }

    @Override
    public void move(double move) {
        //róznica pozycji
        int differenceX = position.x- initialPosition.x ;
        // jesli przekracza zakres ruchu - dobija do konca okienka
        if (differenceX>moveRange){
            hDir=HorizontalDirection.LEFT;
            //zmieniamy kierunek ruchu
        }else if (differenceX<=0){
            hDir=HorizontalDirection.RIGHT;
        }
        super.move(move);
    }

    @Override
    public void paint(Graphics2D g2d){
        super.paint(g2d);

        g2d.drawImage(image, position.x, position.y, null);
    }

    @Override
    public int getHeight() {
        return image.getHeight();
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

    @Override
    public void hit() {
        hp--;
        if (hp==0){
            toBeRemoved=true;
        }

    }
}
