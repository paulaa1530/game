package com.sda.dp.game.view;

import com.sda.dp.game.dispatcher.Dispatcher;
import com.sda.dp.game.interfaces.IFireListener;
import com.sda.dp.game.model.*;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainPanel extends JPanel implements IFireListener {
    private final Color BACKGROUND_COLOR = Color.PINK;
    private final int MAX_MONSTER_SHOTS = 2;
    private final int width;
    private final int height;


    private List<AbstractGameObject> objectList;
    private List<AbstractGameObject> fireList;
    private List<AbstractGameObject> monsterFireList;

    private GameHero hero;
    private Heart heart;
    private GameText gameOverText;

    public MainPanel(int width, int height) {
        super();
        this.width = width;
        this.height = height;

        // create stuff
        objectList = new ArrayList<>();
        fireList = new ArrayList<>();
        monsterFireList = new ArrayList<>();
        heart = new Heart();
        gameOverText=new GameText(width/2, height/2, "GAME OVER!");

        // set stuff
        setBorder(BorderFactory.createLineBorder(Color.black, 2));
        setLayout(null);

        Dispatcher.instance.registerObject(this);
    }


    public void addCharacterIntoGame(AbstractGameObject gameObject) {
        objectList.add(gameObject);
    }

    public void addFireIntoGame(AbstractGameObject gameObject) {
        fireList.add(gameObject);
    }

    public void setGameHero(GameHero hero) {
        this.hero = hero;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.white);
        //rysowanie tła
        g2d.setColor(BACKGROUND_COLOR);
        g2d.fillRect(0, 0, width, height);

        if (hero.getHealthPoints()>0){
            paintWorld(g2d);
        }else {
            paintGameOver(g2d);
        }

    }

    private void paintGameOver(Graphics2D g2d) {
        gameOverText.paint(g2d);
    }

    private void paintWorld(Graphics2D g2d) {
        //rysowanie zycia
        for (int i = 0; i < hero.getHealthPoints(); i++) {
            heart.paint(g2d, i);
        }

        //rysowanie postaci
        AbstractGameObject[] objectsToPaint = new AbstractGameObject[objectList.size()];
        objectsToPaint = objectList.toArray(objectsToPaint);
        for (AbstractGameObject objectToPaint : objectsToPaint) {
            objectToPaint.paint(g2d);
        }

        //rysowanie strzałów
        AbstractGameObject[] firesToPaint = new AbstractGameObject[fireList.size()];
        firesToPaint = fireList.toArray(firesToPaint);

        for (AbstractGameObject fireToPaint : firesToPaint) {
            fireToPaint.paint(g2d);
        }

        //rysowanie strzałów potworów
        AbstractGameObject[] monsterFires = new AbstractGameObject[monsterFireList.size()];
        monsterFires = monsterFireList.toArray(monsterFires);

        for (AbstractGameObject monsterFire : monsterFires) {
            monsterFire.paint(g2d);
        }

        hero.paint(g2d);
    }

    public void move(double move) {
        Random r = new Random();

        if (move > 0) {
            AbstractGameObject[] objectsToPaint = new AbstractGameObject[objectList.size()];
            objectsToPaint = objectList.toArray(objectsToPaint);
            for (AbstractGameObject objectToPaint : objectsToPaint) {
                if (objectToPaint != null) {
                    objectToPaint.move(move);
                }
                if (monsterFireList.size() < MAX_MONSTER_SHOTS) {
                    if ((r.nextInt(1000)) >= (1000-objectList.size())) {
                        //generuj strzał
                        addMonsterFire(objectToPaint.getPositionX(), objectToPaint.getPositionY());
                    }
                }
            }

            AbstractGameObject[] firesToPaint = new AbstractGameObject[fireList.size()];
            firesToPaint = fireList.toArray(firesToPaint);
            for (AbstractGameObject fireToPaint : firesToPaint) {
                if (fireToPaint != null) {
                    fireToPaint.move(move);
                }
            }

            AbstractGameObject[] monsterFiresToMove = new AbstractGameObject[monsterFireList.size()];
            monsterFiresToMove = monsterFireList.toArray(monsterFiresToMove);
            for (AbstractGameObject monsterFireToMove : monsterFiresToMove) {
                if (monsterFireToMove != null) {
                    monsterFireToMove.move(move);
                }
                if (monsterFireToMove.getPositionY()>height){
                    monsterFireList.remove(monsterFireToMove);
                }
            }

            hero.move(move);
        }
    }

    public void checkCollisions() {
        AbstractGameObject[] objectsToMove = new AbstractGameObject[objectList.size()];
        objectsToMove = objectList.toArray(objectsToMove);

        AbstractGameObject[] firesToMove = new AbstractGameObject[fireList.size()];
        firesToMove = fireList.toArray(firesToMove);

        // ConcurrentModificationException
        for (AbstractGameObject strzal : firesToMove) {
            if (!strzal.isToBeRemoved()) {
                for (AbstractGameObject postac : objectsToMove) {
                    if (postac.checkCollision(strzal) && !strzal.isToBeRemoved()) {
                        strzal.setToBeRemoved(true);
                        postac.hit();

                        if (postac.isToBeRemoved())
                            objectList.remove(postac);
                        fireList.remove(strzal);
                        // nie możemy usuwać
                        // usuwanie z iterowanej listy nie jest dozwolone
                    }
                }
            }
        }
        AbstractGameObject[] fires= new AbstractGameObject[monsterFireList.size()];
        fires = monsterFireList.toArray(fires);

        for(AbstractGameObject strzalPotwora : fires){
            if (hero.checkCollision(strzalPotwora)){
                hero.hit();
                monsterFireList.remove(strzalPotwora);
            }
        }


    }




    @Override
    public void addFire(int posX, int posY, double speed, boolean up) {
        addFireIntoGame(new Fire(posX, posY, speed, up));
    }


    public void addMonsterFire(int posX, int posY) {
        addMonsterFireIntoGame(new Fire(posX, posY, 5, false));
    }

    private void addMonsterFireIntoGame(Fire fire) {
        monsterFireList.add(fire);
    }
}
