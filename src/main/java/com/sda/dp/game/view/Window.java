package com.sda.dp.game.view;

import com.sda.dp.game.Main;

import com.sda.dp.game.model.GameHero;
import com.sda.dp.game.model.GameText;
import com.sda.dp.game.model.Monster;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Window extends JFrame {

    private ExecutorService repaintThread = Executors.newSingleThreadExecutor();

    // rozmiar okna
    private static final int SIZE_WIDTH = 1200;
    private static final int SIZE_HEIGHT = 800;
    private static final double FPS = 30;

    private MainPanel mainPanel;
    private Dimension windowSize;

    private boolean isPaused;

    private GameHero hero;

    private long timeRepaint = 0L;
    private double timeBetween = 1000 / FPS;

    public Window() throws HeadlessException {
        super();

        // create stuff
        this.mainPanel = new MainPanel(SIZE_WIDTH, SIZE_HEIGHT);
        this.windowSize = new Dimension(SIZE_WIDTH, SIZE_HEIGHT);

        // set stuff
        setLayout(new BorderLayout());
        getContentPane().add(mainPanel);                                // ustaw panel główny
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);        // co ma się wydarzyć po zamknięciu okna (teraz zamyka aplikację)
        setResizable(false);                                            // zmień jeśli chcesz by okno zmieniało rozmiar
        setSize(this.windowSize);                                       // ustaw rozmiar
        setPreferredSize(this.windowSize);                              // upewnij się co do ustawionego rozmiaru

        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                // if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                //  hero.moveDown();
                // } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                //   hero.moveUp();
                //  } else
                if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                    hero.moveLeft();
                } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                    hero.moveRight();
                } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                    hero.fire();
                } else if (e.getKeyCode() == KeyEvent.VK_P) {
                    isPaused=!isPaused;
                }
                System.out.println("Key pressed: " + e.getKeyCode());
                super.keyPressed(e);
            }

            @Override
            public void keyReleased(KeyEvent e) {
                // if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                //       hero.stopDown();
                //    } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                //        hero.stopUp();
                //     } else
                if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                    hero.stopLeft();
                } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                    hero.stopRight();
                }

                super.keyReleased(e);
            }
        });

        addObjects();

        repaintThread.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    long timeStart, timeEnd;
                    while (true) {
                        timeStart = System.currentTimeMillis();

                        repaint();
                        timeEnd = System.currentTimeMillis();
                        long diff = (long) (timeBetween - (timeEnd - timeStart));
                        if (diff > 0) {
                            try {
                                Thread.sleep(diff);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        if (!isPaused)
                            mainPanel.move(diff / 10.0);
                        mainPanel.checkCollisions();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        pack();
    }

    public void addObjects() {
        hero = new GameHero(500, 680);
        mainPanel.setGameHero(hero);

        loadLevel (1);
    }

    private void loadLevel(int level) {
        //String filename ="levels/level"+level+".slif";
        try (BufferedReader reader = new BufferedReader(new FileReader("levels/level"+level+".silf"))){
            String line = null;
            while ((line=reader.readLine()) != null){
                String[] splits = line.split(" ");
                int poX = Integer.parseInt(splits[0]);
                int poY = Integer.parseInt(splits[1]);
                int dur = Integer.parseInt(splits[2]);
                int range = Integer.parseInt(splits[3]);
                mainPanel.addCharacterIntoGame(new Monster(poX, poY, dur, range));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
